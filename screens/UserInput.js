import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  StyleSheet,
  Modal,
} from 'react-native';
import publicIP from 'react-native-public-ip';
import axios from 'axios';
import MyWebView from './WebView';

export default function UserInput() {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [ipAddress, setIpAddress] = useState('');
  const ipCheck = '42.106.180.61';
  const [firstVerificationSuccess, setFirstVerificationSuccess] = useState(
    false,
  );
  const [correlationId, setCorrelationId] = useState('');
  const [buttonEnable, setButtonEnable] = useState(true);
  function getIpAddress() {
    publicIP()
      .then(ip => {
        setIpAddress(ip);
        setButtonEnable(false);
      })
      .catch(error => {
        console.log(error);
      });
  }
  getIpAddress();

  async function firstUserVerification() {
    try {
      const ipAddress = '42.106.180.61';
      const response = await axios.get(
        `http://10.0.2.2:8000/v2/auth/discover?clientId=adas&userIp=${ipAddress}`,
      );
      // `https://prod.bureau.id/v2/auth/discover?clientId=adas&userIp=${ipAddress}`,
      // 'http://localhost:8000/v2/auth/discover?clientId=adas&userIp=42.106.180.61'

      if (response.data && response.data.supported) {
        setFirstVerificationSuccess(true);
        setCorrelationId(response.data.correlationId);
      }
    } catch (error) {
      console.warn(error.message);
    }
  }

  return firstVerificationSuccess ? (
    <MyWebView phoneNumber={phoneNumber} correlationId={correlationId} />
  ) : (
    <View style={styles.Content}>
      <View style={styles.Header}>
        <Text style={styles.Text}>Bureau</Text>
        <Text style={styles.SubText}>Verify User Credentials</Text>
      </View>
      <View style={styles.Content}>
        <TextInput
          placeholder="Enter your phone number"
          onChangeText={phone => setPhoneNumber(phone)}
          value={phoneNumber}
          keyboardType="numeric"
        />
        <Button
          title="Verify"
          onPress={firstUserVerification}
          disabled={buttonEnable}
        />
        <Text>User's IP Address: {ipAddress}</Text>
        <Text>{correlationId}</Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  Text: {
    fontSize: 24,
    justifyContent: 'center',
  },
  Header: {
    alignItems: 'center',
  },
  SubText: {
    fontSize: 18,
    marginVertical: 10,
  },
  Content: {
    alignContent: 'center',
  },
});
