import React from 'react';
import {View, Dimensions} from 'react-native';
import {WebView} from 'react-native-webview';

const {width, height} = Dimensions.get('screen');

export default function MyWebView({phoneNumber, correlationId}) {
  return (
    <View
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        height,
        width,
      }}>
      <WebView
        source={{
          uri: `http://10.0.2.2:8000/v2/auth/initiate?clientId=adas&correlationId=${correlationId}&redirectUrl=http://localhost/&msisdn=${phoneNumber}`,
        }}
        javaScriptEnabled={true}
      />
    </View>
  );
}

// uri:
// `http://10.0.2.2:8000/v2/auth/initiate?clientId=adas&correlationId=4cb2af8d-acf4-478e-86a3-ff54968b35de&redirectUrl=http://localhost/&msisdn=918086499873`,
