import 'react-native-gesture-handler';
import React, {useState} from 'react';
// import {Root} from 'native-base';
import {
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  StyleSheet,
  Modal,
} from 'react-native';
import MyWebView from './screens/WebView';
import UserInput from './screens/UserInput';
// import InputText from './screens/InputText';
import {NavigationContainer} from '@react-navigation/native';
export default function App() {
  return (
    // <Root>
    <View>
      <NavigationContainer>
        <UserInput />
      </NavigationContainer>
    </View>
    // </Root>
  );
}
const styles = StyleSheet.create({
  Text: {
    fontSize: 24,
    justifyContent: 'center',
  },
  Header: {
    alignItems: 'center',
  },
  SubText: {
    fontSize: 18,
    marginVertical: 10,
  },
  Content: {
    // justifyContent: 'center',
    alignContent: 'center',
  },
});
